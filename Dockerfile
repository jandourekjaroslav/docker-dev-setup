FROM alpine:latest
RUN adduser -g dev -u 2500 -D dev
RUN apk add vim git go ansible terraform zsh python3 py3-pip tmux podman gitlab-runner python3-dev coreutils zsh-vcs fzf sudo bash
RUN apk add prettier minikube --repository=https://dl-cdn.alpinelinux.org/alpine/edge/testing
RUN apk add kubectl --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community
RUN echo "dev ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN sed -i 's/\[engine\]/[engine]\n  remote=true/g' /etc/containers/containers.conf
USER 2500
RUN pip3 install ansible-lint yamllint molecule molecule-podman
WORKDIR /home/dev
ENV SHELL="/bin/zsh"
RUN git init &&\
	git remote add origin https://gitlab.com/jandourekjaroslav/dotfiles.git &&\
	git fetch &&\
	git checkout -f main &&\
	sed -i 's/git@gitlab.com:/https:\/\/gitlab.com\//g' .gitmodules &&\
	git submodule update --init --remote --recursive --merge .bin &&\
	rm -rf .git &&\
	rm -f .git* &&\
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim &&\
	git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf &&\
	~/.fzf/install &&\
	echo 'alias tmux="tmux -2"' >> .zshrc &&\
	sed -i 's/set termguicolors/set notermguicolors/g' .vimrc &&\
	echo 'set background=dark' >> .vimrc &&\
	sed -i 's/✚/+/g' .zshrc &&\
	sed -i 's/●/-/g' .zshrc &&\
	sed -i 's//.k8s:/g' .zshrc &&\
	sed -i 's//.repo:/g' .zshrc &&\
	sed -i 's//.branch: /g' .zshrc &&\
	sed -i 's//.dir: /g' .zshrc &&\
	sed -i 's//>/g' .zshrc &&\
	sudo podman system connection add -d default unix:///run/user/1000/podman/podman.sock
RUN vim +PluginInstall +qall &&\
	vim +GoInstallBinaries +qall
ENTRYPOINT ["/bin/zsh"]
