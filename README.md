# Personalised DEV Env in a Docker Image

## Quickstart

### General

It is a good idea to mount your working directories as needed.
To enable history and config persistence mount overrides into /home/dev are only recommended for specific files otherwise the dev env setup can be completely overridden.

### Linux

```console
podman run --rm -it \
-v /run/user/1000/podman/podman.sock:/run/user/1000/podman/podman.sock \
registry.gitlab.com/jandourekjaroslav/docker-dev-setup:latest
```

- podman and podman.sock can be substituted for docker equivalents as needed
- use `sudo podman` to target the mounted socket of your machine
- for minikube use `--driver=podman --container-runtime=containerd` options

### Windows

Couple of steps are required if you wish to run on windows

1. Install docker-desktop or podman-desktop
1. Go through setup
1. Open a shell into the WSL podman machine `wsl -l --running` after setup can help

   1. (Optional) Run the following to export the socket to other WSL machines

      ```console
      sudo mkdir /mnt/wsl/container && \
      sudo mount --bind /run/user/1000/podman /mnt/wsl/container
      ```

      (Substitute for Docker socket path if using docker desktop instead)

1. Run
   ```console
       podman run --rm -it \
       -v /mnt/wsl/container/podman.sock:/run/user/1000/podman/podman.sock \
       registry.gitlab.com/jandourekjaroslav/docker-dev-setup:latest
   ```

- Make sure to add `--remote` flag if using from a second WSL machine
- podman and podman.sock can be substituted for docker equivalents as needed
- use `sudo podman` to target the mounted socket of your machine
- for minikube use `--driver=podman --container-runtime=containerd` options
